#include<iostream>

using namespace std;
class Node{
	int data;
	Node* link;
	public:
		Node(int data){
			this->data=data;
			this->link=NULL;
		}
	
		int getData(){
			return data;
		}
		Node* getLink(){
			return link;
		}
		void setLink(Node* link){
			this->link=link;
		}
		void setData(){
			this->data=data;
		}
};
class LinkedList{
	Node* head;
	public:
		LinkedList(){
			head->setData(NULL);
			head->setLink(NULL);
			head=NULL;
		}
		void createLL(){
			int x;
			string option;
			Node* current;
			current=head;
			do{
				cout<<"Enter the data:";
				cin>>x;
				Node* node=new Node(x);
				if(head==NULL){
					head=node;
					current=head;
				}
				else{
					current->set_link(node);
					current=node;
				}
			
			cout<<"Do you want to enter numbers:";
			cin>>option;
			}
			while(option=="Yes");
		}
		void display(){
			Node* p=head;
			while (p!=NULL){
				cout<<p->getData()<<endl;
				p=p->getLink();
			}
		}

				
		
int main(){
	/*Node* node1=new Node(5);
	Node* node2=new Node(7);
	Node* node3=new Node(9);
	node1->setLink(node2);
	node2->setLink(node3);
	Node* p=node1;
	cout<<p->getData()<<endl;
	p=p->getLink();
	cout<<p->getData()<<endl;
	p=p->getLink();
	cout<<p->getData()<<endl;
	while (p!=NULL){
		cout<<p->getData()<<endl;
		p=p->getLink();
	}*/
	LinkedList ll;
	ll.createLL();
	ll.display();
	return 0;
	
}
