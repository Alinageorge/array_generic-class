import java.util.Scanner;
public class Matrix{
	private int row;
	private int col;
	private double data[][];
	private String name;
	public Matrix(int rows,int cols){
		row=rows;
		col=cols;
		data=new double[row][col];
	}
	public void setElement(int r,int c,double val){
		data[r][c]=val;

	}
	public double getElement(int r,int c){
		return data[r][c];
	}
	public Matrix add(Matrix mat2){
		Matrix result= new Matrix(row,col);
		int i,j;
		for(i=0;i<result.row;i++){
			for(j=0;j<result.col;j++){
				result.data[i][j]=this.data[i][j]+mat2.data[i][j];
			}
		}
		return result;
	}
	public Matrix subtract(Matrix mat2){
		Matrix result= new Matrix(row,col);
		int i,j;
		for(i=0;i<result.row;i++){
			for(j=0;j<result.col;j++){
				result.data[i][j]=this.data[i][j]-mat2.data[i][j];
			}
		}
		return result;
	}
	public Matrix multiply(Matrix mat2){
		Matrix result=new Matrix(this.row,mat2.col);
		int i,j,k;
		for(i=0;i<result.row;i++){
			for(j=0;j<result.col;j++){
				for(k=0;k<result.col;k++){
					result.data[i][j]+=this.data[i][k]*mat2.data[k][j];
				}
			}
		}
		return result;
	}
	public Matrix transpose(){
		Matrix result=new Matrix(col,row);
		for(int i=0;i<result.row;i++){
			for(int j=0;j<result.col;j++){
				result.data[i][j]=this.data[j][i];
			}
		}
		return result;
	}
			
	public static void main(String [] args){
		
		Scanner obj= new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int rows=obj.nextInt();
		System.out.println("Enter the number of columns");
		int cols=obj.nextInt();
		Matrix mat=new Matrix(rows,cols);
		System.out.println("Enter the values");
		int i,j;
		for (i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				double val=obj.nextDouble();
				mat.setElement(i,j,val);
			}
		}
		System.out.println("The Matrix is:");
		for (i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				System.out.print(mat.getElement(i,j)+" ");
			}
			System.out.println();
		}
		System.out.println("Enter the number of rows of new matrix");
		int row2=obj.nextInt();
		System.out.println("Enter the number of columns of new matrix");
		int col2=obj.nextInt();
		Matrix mat1=new Matrix(row2,col2);
		System.out.println("Enter the values");
		for (i=0;i<row2;i++){
			for(j=0;j<col2;j++){
				double val=obj.nextDouble();
				mat1.setElement(i,j,val);
			}
		}
		if(rows==row2 && cols==col2){
			Matrix sum=mat.add(mat1);
			System.out.println("The Matrix sum is:");
			for (i=0;i<rows;i++){
				for(j=0;j<cols;j++){
					System.out.print(sum.getElement(i,j)+" ");
				}
				System.out.println();
			}
			Matrix diff=mat.subtract(mat1);
			System.out.println("The Matrix Difference is:");
			for (i=0;i<rows;i++){
				for(j=0;j<cols;j++){
					System.out.print(diff.getElement(i,j)+" ");
				}
				System.out.println();		
			}
		}
		else{
			System.out.println("Matrix Addition and Subtracton are not possible!!");
		}
		
		if(cols==row2){
			Matrix prod=mat.multiply(mat1);
			System.out.println("The product of matrices are:");
			for (i=0;i<rows;i++){
				for(j=0;j<col2;j++){
					System.out.print(prod.getElement(i,j)+" ");
				}
				System.out.println();		
			}
		}
		else{
			System.out.println("Matrix Multiplication is not possible!!");
		}
		Matrix trans=mat.transpose();
		for (i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				System.out.print(trans.getElement(i,j)+" ");
			}
			System.out.println();		
		}
		
		
	}
}			
			

	
